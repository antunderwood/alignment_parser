# parse exclude string in the format a..b,c..d,e..f where a,c,e and b,d,f are 
# start and end positions of excluded regions respectively
from Bio import AlignIO
from Bio.Alphabet import generic_dna
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord
from Bio.Align import MultipleSeqAlignment
import argparse, os, sys

def parse_excluded_regions_string(excluded_regions_string): 
    """
    function to parse string of excluded regions and return a list of tuples with start and end of excluded regions
    """
    excluded_ranges = []
    exclusion_range_strings = (excluded_regions_string).split(",")
    for exclusion_range_string in exclusion_range_strings:
        start, end = map(int, (exclusion_range_string).split(".."))
        excluded_ranges.append((start, end))
    return excluded_ranges

def convert_excluded_ranges_to_included_ranges(excluded_ranges, length):
    """
    function to convert excluded ranges to the ranges to include and in the process convert them to zero-based indexing
    """
    start_pos = 0
    included_ranges = []
    for range in excluded_ranges:
        included_ranges.append((start_pos, range[0])) # convert positions to zero-indexing
        start_pos = range[1]
    included_ranges.append((start_pos, length))
    return included_ranges



def extract_included_regions_from_alignment(alignment_filepath, included_ranges):
    alignment = AlignIO.read(alignment_filepath, 'fasta')
    alignments = []
    for record in alignment:
        id = record.id
        original_seq = record.seq
        new_seq = ''
        for range in included_ranges:
            new_seq += original_seq[range[0]:range[1]]
        alignments.append(SeqRecord(new_seq, id=id, description = ''))
    return MultipleSeqAlignment(alignments)

def extract_included_regions_from_alignment_by_slicing(alignment_filepath, included_ranges):
    alignment = AlignIO.read(alignment_filepath, 'fasta')

    for index, range in enumerate(included_ranges):
        if index == 0:
            sliced_alignment = alignment[:, range[0]:range[1]]
        else:
            sliced_alignment = sliced_alignment + alignment[:, range[0]:range[1]]
    return sliced_alignment

def exclude_regions_from_alignment(alignment_filepath, excluded_regions_string):
    excluded_regions = parse_excluded_regions_string(excluded_regions_string)
    included_ranges = convert_excluded_ranges_to_included_ranges(
                            excluded_regions,
                            AlignIO.read(alignment_filepath, 'fasta').get_alignment_length()
                        )
    alignment = extract_included_regions_from_alignment_by_slicing(alignment_filepath, included_ranges)
    return alignment


def write_alignment(alignment, filepath, format = 'fasta'):
    AlignIO.write(alignment, filepath, format)

class ParserWithErrors(argparse.ArgumentParser):
    def error(self, message):
        print('{0}\n\n'.format(message))
        self.print_help()
        sys.exit(2)

    def is_valid_file(self, parser, arg):
        if not os.path.isfile(arg):
            parser.error("The file %s does not exist!" % arg)
        else:
            return arg

def argparser():
    description = """
    A script to parse a filtered VCF and 
    """
    parser = ParserWithErrors(description = description)
    parser.add_argument("-f", "--alignment_filepath", required=True, 
                        help="fasta alignment file path",
                        type=lambda x: parser.is_valid_file(parser, x))
    parser.add_argument("-e", "--excluded_regions", required=True,
                    help="string with excluded regions encoded as a..b,c..d,e..f where a,c,e and b,d,f are start and end positions of excluded regions respectively")

    return parser


if __name__ == '__main__':
    parser = argparser()
    args = parser.parse_args()
    alignment = exclude_regions_from_alignment(args.alignment_filepath, args.excluded_regions)

    output_basename = '.'.join(os.path.basename(args.alignment_filepath).split('.')[:-1]) + ".excluded.fas"
    path_without_base_name = args.alignment_filepath.replace(os.path.basename(args.alignment_filepath), '')

    output_filepath = os.path.join(path_without_base_name, output_basename)
    write_alignment(alignment, output_filepath)