## Alignment Parser

Currently only one function: remove regions from an alignment

### Usage

```
python exclude_regions_from_alignment.py -f path/to/alignment.fas -e 100..200,500..1000
```

### Testing
```
nosetests test_exclude_regions.py
```
