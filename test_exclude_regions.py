from unittest import TestCase
from exclude_regions_from_alignment import *
import tempfile

class TestExcludeRegionsFromAlignment(TestCase):

    def test_parse_excluded_regions_string(self):
        self.assertEqual([(101,200), (801,900)], parse_excluded_regions_string('101..200,801..900'))

    def test_convert_excluded_ranges_to_included_ranges(self):
        self.assertEqual(
            [(0,101), (200,801),(900,1000)],
            convert_excluded_ranges_to_included_ranges([(101,200), (801,900)], 1000)
        )

    def test_extraction_of_regions(self):
        alignment = extract_included_regions_from_alignment('test_data/test_alignment.fas', [(0,3), (5,8), (9,12)])
        self.assertEqual(
            'GGGATTCCC',
            str(alignment[0].seq)
        )

        self.assertEqual(
            'GGAATTCCC',
            str(alignment[1].seq)
        )

    def test_extraction_of_regions_by_slicing(self):
        alignment = extract_included_regions_from_alignment_by_slicing('test_data/test_alignment.fas', [(0,3), (5,8), (9,12)])
        self.assertEqual(
            'GGGATTCCC',
            str(alignment[0].seq)
        )

        self.assertEqual(
            'GGAATTCCC',
            str(alignment[1].seq)
        )

    def test_write_alignment(self):
        alignment = extract_included_regions_from_alignment_by_slicing('test_data/test_alignment.fas', [(0,3), (5,8), (9,12)])
        temp_filepath = tempfile.mkstemp()[1]
        write_alignment(alignment, temp_filepath)
        with open(temp_filepath) as input:
            lines = input.readlines()
            self.assertEqual('>1\n', lines[0])
            self.assertEqual('GGGATTCCC\n', lines[1])
            self.assertEqual('>2\n', lines[2])
            self.assertEqual('GGAATTCCC\n', lines[3])
                